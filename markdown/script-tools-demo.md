# Script tools in ArcGIS

So far we've only run code from notebooks, but lots of times it's handy to be able to run your scripts from inside ArcGIS. This way if you want to run it on two different datasets, you don't have to edit your notebook. Instead, just run the tool and use the ArcGIS dialog to change the parameters. This is also great because you can create custom toolboxes and share them with people, so you could give your toolbox to someone who doesn't know anything about Python and they could still run your script (assuming they know how to use ArcGIS).

You need Python scripts instead of notebooks in order to create tools that run in ArcGIS, though. So you'll be testing your code in a notebook and then copying it into a .py text file. This is possible because the code still works the same, it just needs to be in the form of a Python script instead of a Jupyter Notebook. 

There is one major twist, though. When you run a geoprocessing tool in ArcGIS, you fill out a dialog, right? The filenames and parameters you enter in those dialogs are equivalent to the parameters you've been setting at the tops of your notebooks. The ones in the dialogs tell the tool what files to work with, and the ones in your notebook tell the notebook what files to work with. If you create a custom tool for ArcGIS then the user will enter the needed parameters in a dialog, and you need to be able to get the user's choices in your code. Fortunately, it's not that hard, and you'll see how to do it as you work through this demo.

I thought that a toolbox you created in Pro would work in ArcMap, but I tried it (last semester) and apparently not. ArcMap showed the toolbox, but it couldn't see the tool inside of it. However, ArcGIS Pro could see the tool in a toolbox I created with ArcMap. The second demo shows you Python toolboxes, which *do* work in both ArcGIS Pro and ArcMap (assuming you don't use any tools that are specific to one or the other-- but everything we've done in class so far would also work in ArcMap).

# Demo

Let's look at an extremely simple example that just runs the [Clip](https://pro.arcgis.com/en/pro-app/tool-reference/analysis/clip.htm) tool (this is actually kind of a dumb example because there's no reason for you to build a tool that doesn't do anything more than run one of the Esri ones, but it'll still show you the principles). There will be five main steps to this demo:

1. Create a working Python script that does what you need.
2. Modify the script so that it can get parameters from the tool dialog.
3. Create a tool that uses the script.
4. Test the tool.
5. Edit the tool if needed.

**Please put all files from this demo in your `script_demo` folder!**

## 1. Create an initial working script

It's always a good idea to write your code and make sure it works ***before*** trying to turn it into a tool. It doesn't matter how many times I tell students this, there are always some who try to create the tool without testing their code first and then have a horrible time trying to figure out what's wrong. Don't be like them!

Create a new Jupyter notebook in your `10-toolboxes\script_demo` folder (you'll have less hoops to jump through later if you use Jupyter for this, but you can use ArcGIS if you want). You can call the notebook whatever you want, and even delete it when you're done with the demo if you'd like. Put this code all in one cell (except change the paths to match your computer, obviously):

```python
import arcpy

input_fn = r'D:\classes\NR6920\Assignments\10-toolboxes\data\cities.shp'
boundary_fn = r'D:\classes\NR6920\Assignments\10-toolboxes\data\Cache.shp'
output_fn = r'D:\classes\NR6920\Assignments\10-toolboxes\data\cache_cities.shp'

arcpy.Clip_analysis(in_features=input_fn, clip_features=boundary_fn, out_feature_class=output_fn)
```

Usually `import` statements are the first things in scripts, so that's first here, too. Then it sets parameters, just like you've been doing all semester, and then it runs the code using those parameters. The biggest differences are that the `import` statements area first and everything is in one cell, but those differences don't change how the code works.

Now run the code to make sure it work and save your notebook. 


## 2. Modify the script to accept parameters from ArcGIS

Now you're going to modify your code so that it can be used as an ArcGIS tool. The first thing to do is export your notebook to a script file. Choose `File | Download as | Python (.py)` from the Jupyter menu. Save it as `script_demo\demo.py`.  *(ArcGIS doesn't give you this option, or if it does I didn't see it. If you're using ArcGIS you can either open your notebook in Jupyter and export it from there, or just copy/paste your code into a .py file.)*

![export script](images/export-script.png)

If you have a favorite text editor you can use it to edit this file if you'd like. Otherwise, go to your main Jupyter page and click on `demo.py` to open it in Jupyter.

Remember your three parameters in your script? Instead of setting their values in your code, you want the user to be able to change those through the ArcGIS dialog. You'll see how to set the dialog up later, but right now you're going to worry about getting those values after the user has set them. There are two arcpy functions for doing this, and you need to make sure you use the correct one depending on the type of parameter:

- [GetParameter](https://pro.arcgis.com/en/pro-app/arcpy/functions/getparameter.htm): Use for objects that have their own properties, like a Spatial Reference.
- [GetParameterAsText](https://pro.arcgis.com/en/pro-app/arcpy/functions/getparameterastext.htm): Use for values that can be represented as a string, like a filename.

**These functions DO NOT work in notebooks or in stand-alone Python scripts. If you put them in a notebook the code WILL NOT RUN.** They only work when the script is being used as an ArcGIS tool.
 
In the case of this demo script, all of the parameters are filenames, so `GetParameterAsText` is the appropriate one for all three parameters.

The ArcGIS dialog will pass the parameters to your script in the same order that they appear in the dialog (which you'll create in a bit). In this case, `input_fn` will be the first parameter, `boundary_fn` will be the second, and `output_fn` will be the third. Change the code in your .py file so that it retrieves the parameter values from the dialog **instead** of setting its own, like this (that means replacing the old code, not tacking this onto the end):


```python
import arcpy

input_fn = arcpy.GetParameterAsText(0)    # first tool parameter
boundary_fn = arcpy.GetParameterAsText(1) # second tool parameter
output_fn = arcpy.GetParameterAsText(2)   # third tool parameter

arcpy.Clip_analysis(in_features=input_fn, clip_features=boundary_fn, out_feature_class=output_fn)
```

So now instead of setting those three filenames in the code, the script is going to use `GetParameterAsText()` to get the filenames from the dialog after the user enters them and hits OK.

If you want to show messages to the user, use [AddMessage](https://pro.arcgis.com/en/pro-app/arcpy/functions/addmessage.htm), [AddWarning](https://pro.arcgis.com/en/pro-app/arcpy/functions/addwarning.htm), or [AddError](https://pro.arcgis.com/en/pro-app/arcpy/functions/adderror.htm). For example, you could tell the user that the script is done by adding one more line to the end of the script:

```python
import arcpy

input_fn = arcpy.GetParameterAsText(0)    # first tool parameter
boundary_fn = arcpy.GetParameterAsText(1) # second tool parameter
output_fn = arcpy.GetParameterAsText(2)   # third tool parameter

arcpy.Clip_analysis(in_features=input_fn, clip_features=boundary_fn, out_feature_class=output_fn)

arcpy.AddMessage('Done!')
```

That last line adds a message to the ArcGIS message box. Go ahead and add it to your code, too, and then save your file. You don't have to close it, however.

Now your script is ready to be [turned into a tool](https://pro.arcgis.com/en/pro-app/arcpy/geoprocessing_and_python/adding-a-script-tool.htm), and it will not run any other way. **Make sure you've saved your script before continuing.**


## 3. Turn the script into a tool

Open the ArcGIS project for this week (`10-toolboxes\toolboxes.aprx`).

Since ArcGIS projects automatically create empty script toolboxes called `toolboxes.tbx`, let's put this tool in there. You can find it in the Catalog pane of ArcGIS Pro, under either Toolboxes or Folders. *(If you wanted to create your own toolbox in a custom location, you can do it by right-clicking on a folder in the Catalog pane and choosing `New | Toolbox`.)*

You can add as many tools to a toolbox as you want, but we're only going to add one. Right-click on `toolboxes.tbx` and choose `New | Script`. 

![new script](images/new-script.png)

Give it a name (no spaces allowed) and a label (this is what you see in the toolbox). Use the button to select your `demo.py` file. It's also usually a good idea to check the `Store tool with relative path` box so that if you store your toolbox and script in the same folder then you can give them to someone else and they'll still work.

![toolbox](images/script-toolbox.png)

Now go to the `Parameters` tab. *(If you already hit `OK` instead, expand the toolbox, right-click on your tool, and select `Properties`.)* Now it's time to add your parameters **in the same order that your script expects them**. To add a parameter:

1. Click in the first empty row under `Label` and put in the label that the user will see for the dialog (e.g. `Data to be clipped`).
2. A value will automatically be added to the `Name` box. You can keep what it added, or change it to another valid Python variable name. I changed mine to match the variables names in my script, *but this isn't necessary*.
3. Select the appropriate value from the `Data Type` column. Using a data type of `Feature Layer` allows the user to either select a layer that is already loaded into ArcMap or select another file using a dialog.
4. All of these parameters are `Required`, meaning that the user must provide values for them before they can run the tool.
5. The first two parameters are for datasets that already exist, so their `Direction` is `Input`. The last parameter specifies the name of a dataset to create, so its `Direction` is `Output`.
6. None of the other options are applicable to this demo, so you don't need to worry about them.
7. Click on the empty row at the bottom in order to add more parameters. You need to add all three of the parameters required by your script.

![toolbox](images/script-parameters.png)

The `Validation` tab used when you need your dialog to react to choices that user makes, but that's much more complicated than this demo, so you don't need to worry about it. Go ahead and save your tool by hitting `OK`. Now your tool shows up in the toolbox.

![toolbox](images/script-tool.png)


## 4. Test the tool

Now double-click on your tool, just like you would any other tool, and make sure it runs as expected. It doesn't show the entire path, so you wouldn't know this without looking, but the output file will save in the project's default geodatabase unless you change the save location. You may or may not want to do this. 

The input parameters in the screenshot don't have `.shp` at the end because I have the datasets loaded into ArcGIS and I chose the loaded feature layers rather than using the folder icon to find them on disk. *(You need to turn in a screenshot of this dialog, so go ahead and take one now if it looks how it should. Please save it in your `script_demo` folder.)*

![toolbox](images/run-script-tool.png)

After you click `Run` and the tool finishes, you'll see these options below the `Run` button:

![toolbox](images/script-tool-done.png)

If you click `View Details`, you can see the "Done!" message that you added as the last line of your script. *(If your tool ran and this looks good, take a screenshot of it and save it in your `script_demo` folder.)*

![toolbox](images/script-tool-output.png)


## 5. Edit the tool

You can edit the tool's properties (such as the parameters) later by right-clicking on the tool in the toolbox and choosing `Properties`. 

**If you only need to edit the script, and not the tool properties, then all you have to do is edit and save your script file and your changes will automatically be reflected in the tool.**


## 6. Share the tool

If you want to give this toolbox to someone else to use, just give them the `.tbx` file and the `.py` file that holds the script. Assuming you checked the box for using relative paths in the tool's properties, all they have to do is put them in a location where ArcGIS can find them.


## 7. ArcMap

You can follow this same process to create a tool in ArcMap. Right-click on a folder in ArcCatalog in order to create a toolbox, just like you do in ArcGIS Pro. The dialog to create the parameters *looks* very different, but it has the same options. From my very limited testing, it looks like a toolbox created in ArcMap can be used in ArcGIS Pro, but not the other way around.
