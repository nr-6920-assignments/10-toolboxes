# Python toolboxes

[Python toolboxes](https://pro.arcgis.com/en/pro-app/arcpy/geoprocessing_and_python/creating-a-new-python-toolbox.htm) are a newer kind of toolbox and behave a lot like regular toolboxes, but everything about the toolbox is stored in a Python file. I've seen old-style toolboxes get corrupted and we had to rebuild the toolbox, but since Python toolboxes are just Python scripts, the only way they can be corrupted is if you mess up the code, which can always be fixed. You can also use Python toolboxes created in ArcGIS Pro in ArcMap, unlike the script toolboxes.

The Python toolbox method is a bit more involved, however.

# Demo

Let's create a Python toolbox that contains the same tool as the script toolbox you created with the script-tools-demo. This will have eight main steps.

1. Create a working Python script that does what you need.
2. Create the basic toolbox script.
3. Edit the toolbox properties.
4. Edit the tool properties.
5. Define the tool parameters.
6. Set up the tool processing.
7. Test the tool.
8. Edit the tool if needed. 

**Please put all files from this demo in your `pytool_demo` folder!**

## 1. Create an initial working script

Just like with a regular toolbox, you should make sure you have code that works before trying to turn it into a toolbox. We have that from the script demo, so we won't redo it here.

## 2. Create the basic toolbox script

In ArcGIS Pro, right-click on the `pytool_demo` folder in the Catalog pane and then choose `New | Python Toolbox`.

![toolbox](images/new-python-toolbox.png)

It'll create a file called `New Python Toolbox.pyt` but will automatically be highlighted so you can give it a new name, like `Demo`. If it wasn't highlighted, you can right-click on it in order to rename it.

If you expand your new toolbox, you'll see that it already contains a tool (that doesn't do anything).

![toolbox](images/blank-python-tool.png)

You'll be able to see the new `.pyt` file in your `pytool_demo` folder. Open that file up in Jupyter or your favorite text editor. This is just a Python file with a non-standard extension. The `.pyt` extension tells ArcGIS that it's a Python toolbox, but your computer won't recognize `.pyt` as a Python script and won't try to run it (which is good, because it won't run as a regular script, anyway).

The file already contains some skeleton code, but it won't be syntax highlighted because Jupyter doesn't recognize `.pyt` as a Python file. To get the color-coding back, select `Language | Python` from the menu.

The code contains two classes that you'll need to edit in order to make your toolbox actually do something. (You saw an extremely brief introduction to classes last week.) One of the classes defines the toolbox itself, and the other defines the tool. Each of these `.pyt` files can contain many classes for tools, but only one toolbox class.

## 3. Edit toolbox properties

The first part of the script is the `Toolbox` class. You might remember from last week that `self` is required for class methods. Don't delete `self` anywhere it shows up in this file, but unless you're doing something fancy you probably won't need to add any new references to `self`.

The toolbox class in this file *must* be called `Toolbox`, so don't change its name. You'll want to edit some of its properties, though.

- `self.label` is the name of the toolbox displayed in ArcGIS. Change this to `"Demo Toolbox"`.
- `self.alias` is the short name (no spaces) that's used to identify the toolbox in scripts. Change this to `"dt"`.
- `self.tools` is a list of tools in the toolbox. Change this to `[Clipper]`.

```python
class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Demo Toolbox"
        self.alias = "dt"

        # List of tool classes associated with this toolbox
        self.tools = [Clipper]
```

The `__init__()` method runs when the toolbox is created. So when the toolbox is created, the label and alias will be set. It will also set up a list of the tools contained in the toolbox-- this toolbox will have one tool called `Clipper`, which you'll create in a minute.

Now that your toolbox is configured, it's time to create a tool.

## 4. Edit tool properties

The file also has a class called `Tool`. Rename it to `Clipper` and edit its `__init__` method as shown here:

```python
class Clipper(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Clip Demo"
        self.description = "Clip a feature class"
        self.canRunInBackground = False
```

Because this is called `Clipper`, and `Clipper` is in the list of tools in the `Toolbox` class (from above), this tool will be available in the toolbox. You can add as many tools as you want to the `.pyt` file, but they all need to have unique names, and they all need to be added to the toolbox's `self.tools` list. You can only have one `Toolbox` per `.pyt` file, however.

The `__init__()` method automatically runs when the tool is created and sets the tool's label, description, and other properties. This list of available properties is documented at [Defining a tool in a Python toolbox](https://pro.arcgis.com/en/pro-app/arcpy/geoprocessing_and_python/defining-a-tool-in-a-python-toolbox.htm).


## 5. Define tool parameters

The next step is to [define the tool's parameters](https://pro.arcgis.com/en/pro-app/arcpy/geoprocessing_and_python/defining-parameters-in-a-python-toolbox.htm) in the `getParameterInfo()` method. Right now this method looks like this:

```python
    def getParameterInfo(self):
        """Define parameter definitions"""
        params = None
        return params
```

This method needs to return a list of [Parameter](https://pro.arcgis.com/en/pro-app/arcpy/classes/parameter.htm) objects. This defines the parameters that are shown in the tool's geoprocessing dialog. By default, it returns `None` instead, which means that the tool has no parameters and the user wouldn't be able to enter any when running the tool.

Remember that you need three parameters for your tool, `input_fn`, `boundary_fn`, and `output_fn`. You need to create a Parameter object for each of them, and then return a list of them from this method. The order of the list returned from this is important. The dialog will show the parameters in this order, and this is the order your code needs to expect them after the user makes their selections. 

You want to change `getParameterInfo` to look like this:

```python
    def getParameterInfo(self):
        """Define parameter definitions"""

        # first parameter
        param0 = arcpy.Parameter(
            displayName = 'Data to be clipped',
            name = 'input_fn',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Input')

        # second parameter
        param1 = arcpy.Parameter(
            displayName = 'Clip boundary',
            name = 'boundary_fn',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Input')

        # third parameter
        param2 = arcpy.Parameter(
            displayName = 'Output',
            name = 'output_fn',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Output')

        # return a list of parameters
        params = [param0, param1, param2]
        return params
```

You created three parameters and then returned them in a list. It doesn't matter what you call the variables, so you didn't need to call them `param0`, `param1`, and `param2`. I like using names like that because their order is important and if I have lots of parameters it's easy to tell what the index of each one is. 

Here's the description of the properties you set:

- `displayName` is the label that's shown to the user.
- `name` is the parameter name if you call the tool from a Python script.
- `dataType` is the [type of data](https://pro.arcgis.com/en/pro-app/arcpy/geoprocessing_and_python/defining-parameter-data-types-in-a-python-toolbox.htm) that the parameter will hold.
- `parameterType` is one of 'Required', 'Optional', or 'Derived'. Required means that the user must provide a value for the parameter before they can run the tool and Optional means that they don't (but your code needs to deal with that situation). Derived parameters are created inside your code instead of the user providing a value, and we won't go over them here.
- `direction` is 'Input' or 'Output'. Input parameters are anything the user has to provide, and Output parameters are things that will be created by the tool.

These are the same types of properties you needed to set for the script tool in the first demo, but in that case you set them through a dialog instead of with code.

## 6. Set up tool processing

The other important method for this tool is the `execute()` method. This is where the work gets done. It has a parameter called `parameters`, which is automagically filled with a list of the parameters that the user chose in the dialog, **in the same order that they appear in the list returned by `getParameterInfo()`**. Since it's a list, you can use indexes to get the one you want. These are objects, though, and you need to get the value from each one before you can use it. There are two properties for doing this:

- `value`: Use if the parameter is an object that has its own properties, like a Spatial Reference. 
- `valueAsText`: Use if the parameter is something that can be represented as a string, like a filename.

In the case of our demo, all of the parameters are filenames, so `valueAsText` is the appropriate one for all of them.

Change your `execute` method to look like this:

```python
    def execute(self, parameters, messages):
        """The source code of the tool."""

        # get parameters
        input_fn = parameters[0].valueAsText
        boundary_fn = parameters[1].valueAsText
        output_fn = parameters[2].valueAsText

        # clip input_fn by boundary_fn and save to output_fn
        arcpy.Clip_analysis(input_fn, boundary_fn, output_fn)
        messages.addMessage('Done!')
        return
```

Notice that the code that does the work (the call to `Clip` in this case) is the same as before. The way you get the parameters is a little different than the script tool, but it's similar. Also notice that the parameters are in the same order as you listed them when returning from the `getParameterInfo()` method.

You can use the `messages` parameter that was automatically passed to `execute` to show [messages](https://pro.arcgis.com/en/pro-app/arcpy/geoprocessing_and_python/writing-messages-in-a-python-toolbox.htm) to the user. Here you added a "Done!" message that ArcGIS will display in its message box.

You'll have noticed that there are other methods stubbed out for you (`isLicensed()`, `updateParameters()`, and `updateMessages()`). Don't delete them, even though you're not using them. If you do, you'll break things. You can read more about them [here](https://pro.arcgis.com/en/pro-app/arcpy/geoprocessing_and_python/defining-a-tool-in-a-python-toolbox.htm).

Your file should look like this. **Make sure you save it before continuing.**

```python
import arcpy


class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Demo Toolbox"
        self.alias = "dt"

        # List of tool classes associated with this toolbox
        self.tools = [Clipper]


class Clipper(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Clip Demo"
        self.description = "Clip a feature class"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""

        # first parameter
        param0 = arcpy.Parameter(
            displayName = 'Data to be clipped',
            name = 'input_fn',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Input')

        # second parameter
        param1 = arcpy.Parameter(
            displayName = 'Clip boundary',
            name = 'boundary_fn',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Input')

        # third parameter
        param2 = arcpy.Parameter(
            displayName = 'Output',
            name = 'output_fn',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Output')

        # return a list of parameters
        params = [param0, param1, param2]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""

        # get parameters
        input_fn = parameters[0].valueAsText
        boundary_fn = parameters[1].valueAsText
        output_fn = parameters[2].valueAsText

        # clip input_fn by boundary_fn and save to output_fn
        arcpy.Clip_analysis(input_fn, boundary_fn, output_fn)
        messages.addMessage('Done!')
        return
```


## 7. Test the tool

Once you've saved your `.pyt` file, you can use it like any other tool in ArcGIS.

Right-click on your *toolbox* in the Catalog pane and choose `Refresh`. 

![toolbox](images/refresh-python-toolbox.png)

If the tool disappears when you click `Refresh`, then that means there's a syntax error in your code (right-click and choose `Check Syntax` in order to see it). But assuming it's okay, you'll see the name of the tool change to "Clip Demo".

![toolbox](images/python-tool.png)

Now try running your tool! *(Save a screenshot of this in your `pytool_demo` folder.)*

![toolbox](images/run-python-tool.png)

Again, you can click on "View Details" after running the tool in order to see the messages. *(Save a screenshot of this in your `pytool_demo` folder.)*

![toolbox](images/python-tool-output.png)


## 8. Edit a toolbox

If you make changes to the Python code, you can update the toolbox in ArcGIS by right-clicking on the toolbox and choosing `Refresh`. **If you don't do this, then ArcGIS will still use the old version of the toolbox.**


## 9. Add more tools to the toolbox

As mentioned earlier, you can add as many tools as you want, but you'll need the basic framework for the tool class in order to do this. You could always copy an existing tool and edit it, or if you wanted to start with a clean copy, you can get it [here](https://pro.arcgis.com/en/pro-app/arcpy/geoprocessing_and_python/a-template-for-python-toolboxes.htm) (but notice that this is the template for an entire toolbox, so you'd only want to copy the tool part of it).

Just make sure that each tool class has a unique name (like `Clipper`) and that all of them have been added to the toolbox's `self.tools` property.


## 10. Debug a toolbox

Figuring out why a toolbox doesn't work can be a huge pain, which is why it's always a good idea to test your code as a standalone script first, and then convert it to a tool. You didn't do that as part of this demo, but you tested the same code as part of the script-tools demo.

If a tool isn't visible in the toolbox, here a few things to check:

1. Did you add the tool's class to the toolboxes `self.tools` list?
2. Did you save the `.pyt` file?
3. Did you right-click on the toolbox and choose `Refresh`?
4. If all of those are true, then right-click on the toolbox and choose `Check syntax...`. That'll show you why ArcGIS can't understand the file.

If there is an error while *running* the tool, then the error message will show up in the messages window.

## 11. Share the tool

If you want to give this toolbox to someone else to use, just give them the `.pyt` and associated `.xml` files. All they have to do put them in a location where ArcGIS can find them.

## 12. ArcMap

You can follow this same process to create a tool in ArcMap. Right-click on a folder in ArcCatalog in order to create a Python toolbox, just like you do in ArcGIS Pro. After that, everything is exactly the same. I think that Python toolboxes are interchangeable between ArcGIS Pro and ArcMap, as long as you don't use any geoprocessing tools or code that are specific to one or the other (which is unlikely).
