# Toolboxes homework

*Due Monday, April 5, at midnight*

# Notebook comprehension questions

*Worth 10 points*

Notebooks don't really work for this topic, so there aren't any. Instead, there are two demos that you need to work through before you can do the homework. Each one is worth 5 points.

## Demo 1

Work through the [script tools demo](script-tools-demo.md).

Take screenshots of these two things and save them in your `10-toolboxes\script_demo` folder (because sorting through a million files while grading is a huge pain):

1. Your tool's dialog window where it asks for the parameters when you run it.
2. The processing window showing the "Done!" message.

These are the two that I want (you don't need to circle the "Done" message though):

![run tool](images/run-script-tool.png)
![tool done](images/script-tool-output.png)

### Turn in:
1. The new version of `toolboxes.tbx`
2. The contents of your `script_demo` folder:
   1. `demo.py`
   2. Your two screenshots (.png)


## Demo 2

Work through the [python toolbox demo](python-toolbox-demo.md).

Take screenshots of these two things and save them in your `10-toolboxes\pytool_demo` folder:
- Your tool's dialog window where it asks for the parameters when you run it.
- The processing window showing the "Done!" message.

These are same two dialogs that I wanted for demo 1.

### Turn in:
The contents of your `pytool_demo` folder:
1. Your toolbox (e.g. `Demo.pyt`)
2. Your two screenshots (.png)


# Script problems

*Worth 10 points each*

The end goal of these problems is to have a tool that runs a flow accumulation like you did in the raster-geoprocessing notebook from two weeks ago. It'll discard the flow direction raster, save the final flow accumulation raster, and save a .png representation of the flow accumulation.

## Parameters for your new tool

**The user will provide these four parameters to the tool:**

1. The path to an elevation raster
2. The path to save a flow accumulation raster
3. The path to save a plot of the flow accumulation
4. A cutoff value for plotting the flow accumulation


## Problem 1

The first step in creating a tool is to write a script that works when you've set the parameter values. Once that works, then you turn those parameters into variables and make the whole thing into a tool. So problem 1 is to create a notebook that does what you want, with the parameters set at the top. Make sure your code works before continuing to problem 2.

### Step 1

1. Create a new notebook in the `10-toolboxes\problem1` folder.
2. Import the two modules that you need:
    - `import matplotlib.pyplot as plt`
    - `import arcpy`
3. Check out the Spatial Analyst extension.

### Step 2

Create a variable at the top of your script (after the `import` statements) for each of the four parameters listed above and set them equal to the values below. You can call the variables whatever you want, although the names need to be descriptive.

1. Elevation raster: The full path to `10-toolboxes\data\cach_ned10_clip_int.img`.
2. Flow accumulation raster: The full path for a new output file for a flow accumulation raster (.tif). This file will be created by your code.
3. Flow accumulation plot: The full path for a new output file for a flow accumulation plot (.png). This file will be created by your code.
4. Cutoff: 1000

Make sure you use full paths. **Do not set the workspace!** (You're going to turn this into a tool, and you don't want to set the workspace in a tool because then your tool would only work in that folder. That would be lame, not to mention fairly useless.)

### Step 3

Remember that there are examples in the raster-geoprocessing notebook to do all of these steps (the notebook used filenames, but you need to **use variables**). Add code that:

1. Runs the [FlowDirection](https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/flow-direction.htm) tool on the elevation raster (parameter 1) and stores the output in a variable.
2. Runs the [FlowAccumulation](https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/flow-accumulation.htm) tool on the flow direction output and stores the output in a variable.
3. Saves the output from the flow accumulation tool using the filename specified with your flow accumulation raster parameter (parameter 2).

Test your code and then open the output in ArcGIS to make sure it looks like accumulation.tif from the example in the raster-geoprocessing notebook.

### Step 4

Reclassify your flow accumulation raster so that pixels with values above the cutoff parameter are set to 1 and the ones below the cutoff parameter are set to 0. 

| Old value | New value |
|-----------|-----------|
| > cutoff  | 1         |
| < cutoff  | 0         |

Add more code to your script that:

1. Reclassifies the flow accumulation raster as requested and stores the output in a variable (again, the raster-geoprocessing notebook does something similar).
2. Uses [RasterToNumPyArray](https://pro.arcgis.com/en/pro-app/arcpy/functions/rastertonumpyarray-function.htm) to create a numpy array containing the result from the previous step.
3. Calls [imsave](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.imsave.html) to save the numpy array to an image with the filename specified by parameter 3, using a gray color map. This is a lot like the `imshow()` function you saw in the notebook, but it saves a png instead of displaying one. Because of that, you have to provide a filename as the first parameter. Use this code, but replace `filename` and `data` with the correct values, where `data` is the numpy array that you want to plot:

```py
plt.imsave(filename, data, cmap='gray_r')
```

### Step 5

Test your code again to make sure that it successfully creates the output from steps 3 and 4. Both should look similar to what you saw in the raster-geoprocessing notebook.

### Turn in:
**Please put all of these files in the `10-toolboxes\problem1` folder.**

1. Your notebook (.ipynb)
2. A screenshot of your flow accumulation raster being displayed in ArcGIS (.png -- do not turn in the actual .tif file)
3. Your output .png file

The .png will look like this (assuming a cutoff value of 1000):

![flow accumulation](images/hw_accumulation.png)


## Problem 2

Convert your code from problem 1 into a script tool. 

1. Use `File | Download as | Python (.py)` to save your notebook from problem 1 as a Python script in your `10-toolboxes\problem2` folder.
2. Change the variables from step 2 of problem 1 so that they use `GetParameterAsText` or `GetParameter` (whichever is appropriate) instead of having values provided. Remember that `GetParameterAsText` is for parameters that are **strings**.
3. Use the wizard to create a new tool, similar to the script tools demo. **Use descriptions that are relevant to this problem, though. Don't use the ones from the demo!** Here are the parameter data types:
    1. Raster Layer (make sure you set the correct direction)
    2. Raster Layer (make sure you set the correct direction)
    3. File (make sure you set the correct direction)
        - Set the Filter property to File and put `png` in the box when it asks (this will make it so the user can only select .png files). This wasn't in the demo, but there is a Filter option when you're defining the parameters.
    4. Long (this is a large integer)
4. Run your tool with a **cutoff value of 3000**.

### Turn in:
**Please put all of these files in a `10-toolboxes\problem2` folder.**

1. Your script (.py)
2. Your toolbox (.tbx)
3. Your output .png file
4. A screenshot of your tool's dialog window where it asks for the parameters when you run it (.png -- do not turn in the actual .tif file)
5. A screenshot of the processing details window showing that the tool ran successfully (.png)

These are the same two dialogs I wanted screenshots of for the demos. **Make sure that your output .png and your screenshots are for when you ran your tool with a cutoff of 3000.**

## Problem 3

Convert your code from problem 1 into a Python tool. 

1. Follow the python tool demo outline, but **use descriptions that are relevant to this problem. Don't use the ones from the demo!** Also make sure you use your four parameters from problem 1 instead of the ones in the demo. Don't forget to add the fourth parameter to the `params` list that gets returned from `getParameterInfo()`. Here are the parameter data types:
    1. GPRasterLayer (make sure you set the correct direction)
    2. GPRasterLayer (make sure you set the correct direction)
    3. DEFile (make sure you set the correct direction)
        - To force the user to select a .png file, add a filter after you create the parameter, as shown below (assuming your parameter is called `param2`):  
        `param2.filter.list = ['png']`
        - This code will still be inside the `getParameterInfo()` method, but after you create `param2` (or whatever you call it) and before you return the list of parameters from the method.
    4. GPLong
2. Copy the code from problem 1, steps 2-4, and put it in the `execute()` method for your tool. Change the code that gets the parameters so it uses `parameters[n].valueAsText` or `parameters[n].value` (whichever is appropriate) instead of having values provided.
3. Put the `import` statement for matplotlib at the beginning of the script.
4. Run your tool with a **cutoff value of 200**.

### Turn in:
**Please put all of these files in a `10-toolboxes\problem3` folder.**

1. Your toolbox (.pyt)
2. Your output .png file
3. A screenshot of your tool's dialog window where it asks for the parameters when you run it (.png -- do not turn in the actual .tif file)
4. A screenshot of the processing window showing that the tool ran successfully (.png)

These are the same two dialogs I wanted screenshots of for the demos. **Make sure that your output .png and your screenshots are for when you ran your tool with a cutoff of 200.**
